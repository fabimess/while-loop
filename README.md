Editor: vim
Compiler: gcc


Ausdruck Aufgabe 11 Erklärung:

In einer while Schleife wird dem Identifier "Zeichen" der Wert der fgetc Funktion zugewiesen und überprüft, ob dieser ungleich EOF ist. Damit wird geguckt, ob Fehler aufgetreten sind oder man am Ende des Dokuments angekommen ist.

Sind die Bedingungen der while Schleife erfüllt, druckt das Programm den Text in der Textdatei "pp8datei".

